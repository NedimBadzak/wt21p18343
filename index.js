const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
const Sequelize = require('sequelize');
const sequelize = require("./baza");

app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public/html/'));
app.use(express.static(__dirname + '/public/'));

const Vjezba = require('./models/Vjezba.js')(sequelize);
const Zadatak = require('./models/Zadatak.js')(sequelize);
Vjezba.hasMany(Zadatak, {as: 'zadaci'});
Zadatak.belongsTo(Vjezba);
Vjezba.sync();
Zadatak.sync();
const Student = require('./models/Student.js')(sequelize);
const Grupa = require('./models/Grupa.js')(sequelize);
Student.belongsTo(Grupa);
Grupa.hasMany(Student, {as: 'studenti'});
Student.sync();
Grupa.sync();


Array.prototype.forEachAsync = async function (fn) {
    for (let t of this) {
        await fn(t)
    }
}

async function truncTabelu(tabela) {
    const t = await sequelize.transaction();
    try {
        await sequelize.query('TRUNCATE `' + tabela + '`', {transaction: t});
    } catch (e) {
        await sequelize.query('DELETE FROM `' + tabela + '`', {transaction: t});
        await sequelize.query('ALTER TABLE `' + tabela + '` AUTO_INCREMENT = 1;', {transaction: t});
    }

    await t.commit();
}

    // app.get('/delete/:tabela', async function (req, res) {
    //     truncTabelu(req.params.tabela).then(() => {
    //         res.send('Gotovo');
    //     });
    // });

    app.get('/vjezbe/', function (req, res) {
        let brojZadatakaPoVjezbi = [];
        Vjezba.findAll().then(function (vjezbe) {
            // vjezbe.forEach(function (row) {
            //     row.getZadaci().then(function (zadaci) {
            //         res.json({
            //             brojVjezbi: vjezbe.length,
            //             brojZadataka: zadaci.map(function (zadatak) {
            //                 return zadatak.brojZadataka;
            //             })
            //         });
            //     });
            // });

            vjezbe.forEachAsync(async function (row) {
                await row.getZadaci().then(function (zadaci) {
                    brojZadatakaPoVjezbi.push(zadaci.length);
                });
            }).then(() => {
                res.json({
                    brojVjezbi: vjezbe.length,
                    brojZadataka: brojZadatakaPoVjezbi
                });
            });
        })
    });

    app.post('/vjezbe', async function (req, res) {
        let data = req.body.brojVjezbi + "";
        let zadaci = req.body.brojZadataka;
        let greska = {
            status: "error",
            data: "Pogrešan parametar "
        }
        let pravilneVjezbe = req.body.brojVjezbi >= 1 &&
            req.body.brojVjezbi <= 15 &&
            Number.isInteger(req.body.brojVjezbi);
        if (!pravilneVjezbe) greska.data += "brojVjezbi";


        let nepravilanNeki = false;

        for (let i = 0; i < req.body.brojVjezbi; i++) {
            if (zadaci[i] < 0 || zadaci[i] > 10) {
                if (!pravilneVjezbe || nepravilanNeki) greska.data += ",";
                greska.data += "z" + i;
                nepravilanNeki = true;
            }
            data += ', ' + zadaci[i];
        }

        let pravilniZadaci = zadaci.length === req.body.brojVjezbi;
        if (!pravilniZadaci) {
            if (!pravilneVjezbe || nepravilanNeki) greska.data += ",";
            greska.data += "brojZadataka";
        }

        if (!pravilniZadaci || !pravilneVjezbe || nepravilanNeki) {
            res.json(greska);
        } else {
            await truncTabelu('Vjezbas').then(() => truncTabelu('Zadataks').then(() => console.log('Gotovo')));
            let vjezbe = [];
            for (let i = 0; i < req.body.brojVjezbi; i++) vjezbe.push({imeVjezbe: i + 1});
            await Vjezba.bulkCreate(vjezbe).then(function (vjezbe) {
                vjezbe.forEachAsync(async function (vjezba) {
                    let noviZadaci = [];
                    for (let i = 0; i < zadaci[vjezba.imeVjezbe - 1]; i++) {
                        noviZadaci.push({
                            imeZadatka: i + 1,
                            VjezbaId: vjezba.id
                        });
                    }
                    await Zadatak.bulkCreate(noviZadaci);
                }).then(function () {
                    res.json({
                        brojVjezbi: req.body.brojVjezbi,
                        brojZadataka: zadaci
                    })
                });
            });
        }
    });


    app.get('/', function (req, res) {
        res.sendFile(__dirname + '/public/html/unosVjezbi.html');
    });

    app.post('/student', async function (req, res) {
        // let data = req.body.ime + "," + req.body.prezime + "," + req.body.brojVjezbi + ",";

        let ime = req.body.ime;
        let prezime = req.body.prezime;
        let grupa = req.body.grupa;
        let index = req.body.index;

        if(index == null || index === ""){
            res.json({
                status: "Nije unesen index studenta!"
            });
            return;
        }
        const [model, created] = await Student.findOrCreate({where: {index: index}});
        if (created) {
            const [grupaModel, created] = await Grupa.findOrCreate({where: {imeGrupe: grupa}});
            model.update({
                ime: ime,
                prezime: prezime,
                grupa: grupa,
                index: index,
                GrupaId: grupaModel.id
            }).then(function () {
                res.json({
                    status: "Kreiran student!",
                })
            });
        } else {
            res.json({
                status: "Student sa indexom " + index + " već postoji!",
            });
        }
    });

    app.put('/student/:index', async function (req, res) {
        if(req.params.index == null) {
            res.json({
                status: "Nije unesen index studenta!"
            });
            return;
        }
        if(req.body.grupa == null || req.body.grupa === "") {
            res.json({
                status: "Nije unesena grupa studenta!"
            });
            return;
        }
        let index = req.params.index;

        const student = await Student.findOne({where: {index: index}});
        if (student == null) {
            res.json({
                status: "Student sa indexom " + index + " ne postoji",
            })
        } else {
            const [grupaModel, created] = await Grupa.findOrCreate({where: {imeGrupe: req.body.grupa}});
            student.update({
                grupa: grupaModel.imeGrupe,
                GrupaId: grupaModel.id
            }).then(function () {
                res.json({
                    status: "Promjenjena grupa studentu " + index,
                })
            });
        }
        // console.log(student);
    })

    app.post('/batch/student', function (req, res) {
        let studenti = req.body.split('\n');
        var brojUspjesnih = 0;
        var neuspjesniStudenti = [];

        studenti.forEachAsync(async function (student) {
            const [ime, prezime, index, grupa] = student.split(",");
            if(index == null || index === ""){
                res.json({
                    status: "Nije unesen index studenta!"
                });
                return;
            }
            const [model, created] = await Student.findOrCreate({where: {index: index}});
            if (created) {
                brojUspjesnih++;
                const [grupaModel, created] = await Grupa.findOrCreate({where: {imeGrupe: grupa}});
                await model.update({
                    ime: ime,
                    prezime: prezime,
                    grupa: grupaModel.imeGrupe,
                    index: index,
                    GrupaId: grupaModel.id
                });
            } else {
                neuspjesniStudenti.push(index);
            }
        }).then(() => {
            if (neuspjesniStudenti.length > 0) {
                res.json({
                    status: "Dodano " + brojUspjesnih + " studenata, a studenti " + neuspjesniStudenti.join(",") + " već postoje!"
                })
            } else {
                res.json({
                    status: "Dodano " + brojUspjesnih + " studenata!",
                })
            }
        });



    });

    // app.listen(3000);
module.exports = app.listen(3000);