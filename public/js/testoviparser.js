let TestoviParser = (function () {
        function validate(jsonic) {
            let obj = "";
            try {
                obj = JSON.parse(jsonic);
            } catch (e) {
                return false;
            }
            if (obj === null) return false;
            //Check if json contains all required keys
            let keys = ["stats", "tests", "failures", "passes", "pending"];
            for (let i = 0; i < keys.length; i++) {
                if (!obj.hasOwnProperty(keys[i])) {
                    return false;
                }
            }
            keys = ["suites", "tests", "passes", "failures", "pending", "duration", "start", "end"];
            for (let i = 0; i < keys.length; i++) {
                if (!obj.stats.hasOwnProperty(keys[i])) {
                    return false;
                }
            }
            keys = ["title", "fullTitle", "duration", "file", "err", "currentRetry", "speed"];
            for (let i = 0; i < keys.length; i++) {
                for (let j = 0; j < obj.tests.length; j++) {
                    if (!obj.tests[j].hasOwnProperty(keys[i])) {
                        return false;
                    }
                }
                for (let j = 0; j < obj.passes.length; j++) {
                    if (!obj.passes[j].hasOwnProperty(keys[i])) {
                        return false;
                    }
                }
                for (let j = 0; j < obj.failures.length; j++) {
                    if (!obj.failures[j].hasOwnProperty(keys[i])) {
                        return false;
                    }
                }
            }
            return true;
        }

        const dajTacnost = function (jsonalo) {
            var obj = "";
            if(validate(jsonalo)){
                obj = JSON.parse(jsonalo);
            } else {
                return {
                    "tacnost": "0%",
                    "greske": ["Testovi se ne mogu izvršiti"]
                };
            }


            var stats = obj.stats;
            if (stats.tests === stats.passes) {
                return {
                    "tacnost": "100%",
                    "greske": []
                };
            } else {

                var tacnost = (stats.passes / stats.tests) * 100;
                if (!Number.isInteger(tacnost)) {
                    tacnost = tacnost.toFixed(1);
                }

                var returner = {
                    "tacnost": tacnost.toString() + "%",
                    "greske": []
                };

                var greske = obj.failures;
                for (var i = 0; i < greske.length; i++) {
                    returner.greske.push(greske[i].fullTitle);
                }
                return returner;
            }
        }
        const porediRezultate = function (rezultat1, rezultat2) {
            var obj1 = JSON.parse(rezultat1);
            var obj2 = JSON.parse(rezultat2);

            var i = 0;
            for (i = 0; i < obj1.tests.length; i++) {
                var nasao = false;
                for (let j = 0; j < obj2.tests.length; j++) {
                    if (obj1.tests[i].fullTitle === obj2.tests[j].fullTitle) {
                        nasao = true;
                        break;
                    }
                }
                if (!nasao) break;
            }

            var tacnost = 0;
            var returner = {
                "promjena": tacnost.toString() + "%",
                "greske": []
            };


            if (i !== obj1.tests.length) {
                console.log("Novi test");
                var brojac1Bez2 = 0;
                var padalo1 = 0;
                let greskeJedinice = [];
                for (let j = 0; j < obj1.tests.length; j++) {
                    var nasao = false;
                    for (let k = 0; k < obj2.tests.length; k++) {
                        if (obj1.tests[j].fullTitle === obj2.tests[k].fullTitle) {
                            nasao = true;
                            break;
                        }
                    }
                    if (!nasao) {
                        brojac1Bez2++;
                        for (let k = 0; k < obj1.failures.length; k++) {
                            if (obj1.failures[k].fullTitle === obj1.tests[j].fullTitle) {
                                greskeJedinice.push(obj1.failures[k].fullTitle);
                                padalo1++;
                            }
                        }
                    }
                }
                let padalo2 = obj2.stats.failures;
                tacnost = (padalo1 + padalo2) / (padalo1 + obj2.stats.tests) * 100;

                let greskeDvojke = [];
                for (let j = 0; j < obj2.tests.length; j++) {
                    // var nasao = false;
                    // for (let k = 0; k < obj1.tests.length; k++) {
                    //     if (obj2.tests[j].fullTitle === obj1.tests[k].fullTitle) {
                    //         console.log("Obj 2:" + obj2.tests[j].fullTitle);
                    //         nasao = true;
                    //         break;
                    //     }
                    // }
                    // if (!nasao) {
                    for (let k = 0; k < obj2.failures.length; k++) {
                        console.log("k ova: " + obj2.failures[k].fullTitle);
                        console.log("j ova: " + obj2.tests[j].fullTitle);
                        if (obj2.failures[k].fullTitle === obj2.tests[j].fullTitle) {
                            console.log(obj2.failures.length);
                            greskeDvojke.push(obj2.failures[k].fullTitle);
                        }
                        // }
                    }
                }

                if (!Number.isInteger(tacnost)) {
                    tacnost = tacnost.toFixed(1);
                }


                greskeJedinice.sort((a, b) => a > b ? 1 : -1);
                console.log(greskeJedinice);
                for (i = 0; i < greskeJedinice.length; i++) {
                    returner.greske.push(greskeJedinice[i]);
                }

                console.log(greskeDvojke);
                greskeDvojke.sort((a, b) => a > b ? 1 : -1);
                for (i = 0; i < greskeDvojke.length; i++) {
                    returner.greske.push(greskeDvojke[i]);
                }

                returner.promjena = tacnost.toString() + "%";
                return returner;
            } else {

                if (obj2.stats.tests === obj2.stats.passes) {
                    tacnost = 100;
                    returner.promjena = tacnost.toString() + "%";
                } else {

                    tacnost = (obj2.stats.passes / obj2.stats.tests) * 100;
                    if (!Number.isInteger(tacnost)) {
                        tacnost = tacnost.toFixed(1);
                    }


                    var greske = obj2.failures;
                    for (var i = 0; i < greske.length; i++) {
                        returner.greske.push(greske[i].fullTitle);
                    }
                    returner.greske.sort((a, b) => a < b ? 1 : -1);
                    returner.promjena = tacnost.toString() + "%";
                }
                return returner;
            }
        }

        return {
            dajTacnost: dajTacnost,
            porediRezultate: porediRezultate
        }
    }()
);