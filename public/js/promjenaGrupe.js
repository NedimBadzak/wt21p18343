window.onload = () => {
    const posalji = document.getElementById('posalji');
    posalji.addEventListener('click', () => {
        const index = document.getElementById('index');
        const grupa = document.getElementById('grupa');
        StudentAjax.postaviGrupu(
            index.value,
            grupa.value,
            (err, data) => {
                if (err) {
                    console.log(err.status);
                    document.getElementById('ajaxstatus').innerHTML = err.status;
                } else {
                    console.log(data.status);
                    document.getElementById('ajaxstatus').innerHTML = data.status;
                }
            });
    });
}