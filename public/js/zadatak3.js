let assert = chai.assert;
chai.should();
describe('VjezbeAjax', function () {
    describe('dohvatiPodatke()', function () {
        beforeEach(function() {
            this.xhr = sinon.useFakeXMLHttpRequest();

            this.requests = [];
            this.xhr.onCreate = function(xhr) {
                this.requests.push(xhr);
            }.bind(this);
        });

        afterEach(function() {
            this.xhr.restore();
        });

        it('Test loseg brojaVjezbi uzimanja podataka', function () {
            var data = {brojVjezbi: 10, brojZadataka: [4, 5, 3, 4] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.dohvatiPodatke(function(err, result) {
                assert(result !== data, "Podaci se ne poklapaju");
                assert(err !== null, "Nije dobijen error");
            });
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        })

        it('Test loseg broja zadataka', function (done) {
            var objekat = {"brojVjezbi":10,"brojZadataka":[4,5,3,4]}
            var objekatJSON = JSON.stringify(objekat)

            VjezbeAjax.dohvatiPodatke(function(err, result) {
                assert.equal(result, null, "Podaci se ne poklapaju");
                assert.deepEqual(err, {status:'error', data: 'Pogrešan parametar brojZadataka'}, "Nije dobijen error");
                done();
            });
            this.requests[0].respond(200, { "Content-Type": "text/json" }, objekatJSON);
        })

        it('Test loseg broja zadataka uzimanja podataka', function () {
            var data = {brojVjezbi: 4, brojZadataka: [4, 5, 3, 4, 5] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.dohvatiPodatke(function(err, result) {
                assert(result !== data, "Podaci se ne poklapaju");
                assert.deepEqual(err, {status:'error', data: 'Pogrešan parametar brojZadataka'}, "Nije dobijen error");
            });
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        })

        it('Test loseg pojedinacnog broja uzimanja podataka', function () {
            var data = {brojVjezbi: 10, brojZadataka: [4, 15, 3, 4] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.dohvatiPodatke(function(err, result) {
                assert(result !== data, "Podaci se ne poklapaju");
                assert.deepEqual(err, {status:'error', data: 'Pogrešan parametar z1,brojZadataka'}, "Nije dobijen error");
            });
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        })

        it('Test sve lose uzimanja podataka', function () {
            var data = {brojVjezbi: 20, brojZadataka: [4, 15, 3, 4] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.dohvatiPodatke(function(err, result) {
                assert(result !== data, "Podaci se ne poklapaju");
                assert.deepEqual(err, {status:'error', data: 'Pogrešan parametar brojVjezbi,z1,brojZadataka'}, "Nije dobijen error");
            });
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        })

        it('Test dobrog uzimanja podataka', function () {
            var data = {brojVjezbi: 4, brojZadataka: [4, 5, 3, 4] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.dohvatiPodatke(function(err, result) {
                assert(JSON.stringify(result) === dataJson, "Podaci se ne poklapaju");
                assert(err === null, "Dobijen error");
            });
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        })


    });
    describe('posaljiPodatke()', function () {

            beforeEach(function() {
                this.xhr = sinon.useFakeXMLHttpRequest();

                this.requests = [];
                this.xhr.onCreate = function(xhr) {
                    this.requests.push(xhr);
                }.bind(this);
            });

            afterEach(function() {
                this.xhr.restore();
            });


        it('Test slanja podataka sa dobrim unosom', function() {
            var data = {brojVjezbi: 4, brojZadataka: [4, 5, 3, 4] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.posaljiPodatke( data, function(err, result) {
                assert(JSON.stringify(result) === dataJson, "Podaci se ne poklapaju");
                assert(err === null, "Dobijen error");
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        });

        it('Test slanja podataka sa losim unosom broja zadataka', function() {
            var data = {brojVjezbi: 14, brojZadataka: [4, 5, 3, 4] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.posaljiPodatke( data, function(err, result) {
                assert(result !== data, "Podaci se ne poklapaju");
                assert.deepEqual(err, {status:'error', data: 'Pogrešan parametar brojZadataka'}, "Nije dobijen error");
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        });

        it('Test slanja podataka sa losim unosom broja zadataka', function() {
            var data = {brojVjezbi: 16, brojZadataka: [4, 5, 3, 4, 4, 5, 3, 4, 4, 5, 3, 4, 4, 5, 3, 4] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.posaljiPodatke( data, function(err, result) {
                assert(result !== data, "Podaci se ne poklapaju");
                assert.deepEqual(err, {status:'error', data: 'Pogrešan parametar brojVjezbi'}, "Nije dobijen error");
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        });

        it('Test slanja podataka sa losim unosom broja zadataka', function() {
            var data = {brojVjezbi: 4, brojZadataka: [4, 5, 3, 4, 5] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.posaljiPodatke( data, function(err, result) {
                assert(result !== data, "Podaci se ne poklapaju");
                assert.deepEqual(err, {status:'error', data: 'Pogrešan parametar brojZadataka'}, "Nije dobijen error");
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        });

        it('Test slanja podataka sa losim unosom jednog broja zadataka', function() {
            var data = {brojVjezbi: 4, brojZadataka: [4, 15, 3, 4] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.posaljiPodatke( data, function(err, result) {
                assert(result !== data, "Podaci se ne poklapaju");
                assert.deepEqual(err, {status:'error', data: 'Pogrešan parametar z1'}, "Nije dobijen error");
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        });

        it('Test slanja podataka sa losim unosom vise broja zadataka', function() {
            var data = {brojVjezbi: 4, brojZadataka: [4, 15, 23, 4] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.posaljiPodatke( data, function(err, result) {
                assert(result !== data, "Podaci se ne poklapaju");
                assert.deepEqual(err, {status:'error', data: 'Pogrešan parametar z1,z2'}, "Nije dobijen error");
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        });

        it('Test slanja podataka sa losim unosom svega', function() {
            var data = {brojVjezbi: 24, brojZadataka: [4, 15, 3, 4] };
            var dataJson = JSON.stringify(data);
            VjezbeAjax.posaljiPodatke( data, function(err, result) {
                assert(result !== data, "Podaci se ne poklapaju");
                assert.deepEqual(err, {status:'error', data: 'Pogrešan parametar brojVjezbi,z1,brojZadataka'}, "Nije dobijen error");
            });
            this.requests[0].requestBody.should.equal(dataJson);
            this.requests[0].respond(200, { "Content-Type": "text/json" }, dataJson);
        });

    })

    describe('iscrtajVjezbe() i iscrtajZadatke()', function () {
        it('Test iscrtavanja vjezbi pogresan objekat tj brojVjezbi', function () {
            var dom = document.createElement('div');
            VjezbeAjax.iscrtajVjezbe(dom, {brojVjezbi: 10, brojZadataka: [4, 5, 3, 4, 5]});
            assert(dom.innerHTML === "", "Iscrtano mada nije trebalo biti");
        })

        it('Test iscrtavanja vjezbi pogresan objekat tj brojZadatka', function () {
            var dom = document.createElement('div');
            VjezbeAjax.iscrtajVjezbe(dom, {brojVjezbi: 5, brojZadataka: [20, 5, 3, 4, 5]});
            assert(dom.innerHTML === "", "Iscrtano mada nije trebalo biti");
        })

        it('Test iscrtavanja vjezbi pogresan objekat tj brojZadataka', function () {
            var dom = document.createElement('div');
            VjezbeAjax.iscrtajVjezbe(dom, {brojVjezbi: 5, brojZadataka: [2, 5, 3, 4, 5, 8, 7]});
            assert(dom.innerHTML === "", "Iscrtano mada nije trebalo biti");
        })

        it('Test iscrtavanja vjezbi dobar objekat', function () {
            var dom = document.createElement('div');
            VjezbeAjax.iscrtajVjezbe(dom, {brojVjezbi: 5, brojZadataka: [4, 5, 3, 4, 5]});
            assert(dom.innerHTML !== "", "Nije iscrtano mada je trebalo biti");
        })

        it('Test iscrtavanja vjezbi dobar objekat, provjera teksta vjezbe', function () {
            var dom = document.createElement('div');
            VjezbeAjax.iscrtajVjezbe(dom, {brojVjezbi: 5, brojZadataka: [4, 5, 3, 4, 5]});
            assert(dom.innerHTML.includes("Vjezba 3"), "Nije iscrtano mada je trebalo biti");
            assert(!dom.innerHTML.includes("Vjezba 7"), "Iscrtano mada nijje trebalo biti");
        })

        it('Test iscrtavanja vjezbi dobar objekat, provjera zadataka unutar vjezbe', function () {
            var dom = document.createElement('div');
            VjezbeAjax.iscrtajVjezbe(dom, {brojVjezbi: 5, brojZadataka: [4, 5, 3, 4, 5]});
            //Get all children of the div
            var children = dom.children;
            //Get the second child
            var child = children[1];
            //Get all children of the second child
            var children2 = child.children;
            children2[0].click();
            assert(children2[1].children.length === 5, "Pogresan broj zadataka u vjezbi");
        })

        it('Test iscrtavanja vjezbi dobar objekat, provjera teksta zadatka unutar vjezbe', function () {
            var dom = document.createElement('div');
            VjezbeAjax.iscrtajVjezbe(dom, {brojVjezbi: 5, brojZadataka: [4, 5, 3, 4, 5]});
            //Get all children of the div
            var children = dom.children;
            //Get the second child
            var child = children[1];
            //Get all children of the second child
            var children2 = child.children;
            children2[0].click();
            assert(children2[1].children[0].innerHTML === "Zadatak 1", "Pogresan broj zadataka u vjezbi");
        })

        it('Test iscrtavanja vjezbi dobar objekat, provjera teksta zadatka unutar vjezbe', function () {
            var dom = document.createElement('div');
            VjezbeAjax.iscrtajVjezbe(dom, {brojVjezbi: 5, brojZadataka: [4, 5, 3, 4, 5]});
            //Get all children of the div
            var children = dom.children;
            //Get the second child
            var child = children[1];
            //Get all children of the second child
            var children2 = child.children;
            children[0].children[0].click();
            children2[0].click();
            assert(children[0].children[1].style.display === "none", "Nije nestao zadatak 1 mada je trebao");
            children[0].children[0].click();
            assert(children[0].children[1].style.display == "grid", "Nije se pojavio zadatak 1 mada je trebao");
            assert(children2[1].style.display === "none", "Nije nestao zadatak 1 mada je trebao");
        })

    })

    describe('dodajInputPolja()', function () {

        it('Test dodavanja input polja, sve ok', function () {
            var dom = document.createElement('div');
            VjezbeAjax.dodajInputPolja(dom, 7);
            //Get all children of the div
            var children = dom.children;
            //Get all children of type input
            var unosi = []
            Array.from(children).forEach(function (child) {
                if(RegExp("^z[0-9]$").test(child.id)) {
                    unosi.push(child);
                }
            });
            assert(unosi.length === 7, "Pogresan broj unosa");
        })

        it('Test dodavanja input polja, provjera broja kao i dupliciranja i svih neg slucajeva', function () {
            var dom = document.createElement('div');
            var children = dom.children;

            VjezbeAjax.dodajInputPolja(dom, 7);
            //Get all children of the div
            //Get all children of type input
            var unosi = []
            Array.from(children).forEach(function (child) {
                if(RegExp("^z[0-9]$").test(child.id)) {
                    unosi.push(child);
                }
            });
            assert(unosi.length === 7, "Pogresan broj unosa");


            VjezbeAjax.dodajInputPolja(dom, 1);
            unosi = []
            Array.from(children).forEach((child) => {
                if(RegExp("^z[0-9]$").test(child.id)) {
                    unosi.push(child);
                }
            });
            assert(unosi.length === 1, "Pogresan broj unosa, duplicira se");

            VjezbeAjax.dodajInputPolja(dom, 0);
            unosi = []
            Array.from(children).forEach((child) => {
                if(RegExp("^z[0-9]$").test(child.id)) {
                    unosi.push(child);
                }
            });
            assert(unosi.length === 0, "Pogresan broj unosa, pojavilo se nesto, a ne treba");


            VjezbeAjax.dodajInputPolja(dom, 50);
            unosi = []
            Array.from(children).forEach((child) => {
                if(RegExp("^z[0-9]$").test(child.id)) {
                    unosi.push(child);
                }
            });
            assert(unosi.length === 0, "Pogresan broj unosa, pojavilo se");
        })


        it('Provjera dodavanja input polja, provjera negativnog broja vjezbi', function () {
            var dom = document.createElement('div');
            VjezbeAjax.dodajInputPolja(dom, -1);
            //Get all children of the div
            var children = dom.children;
            //Get all children of type input
            var unosi = []
            Array.from(children).forEach(function (child) {
                if(RegExp("^z[0-9]$").test(child.id)) {
                    unosi.push(child);
                }
            });
            assert(unosi.length === 0, "Pogresan broj unosa");
        })

        // it('Provjera dodavanja input polja, provjera nevazeceg DOMa', function () {
        //     var dom = null;
        //     VjezbeAjax.dodajInputPolja(dom, 5);
        //     assert(dom === null, "Rad sa nevazecem DOMom");
        // })

    })

});