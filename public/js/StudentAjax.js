let StudentAjax = (function() {

    const dodajStudenta = function(student, fnCallback) {
        //student je Object, a fnCallback je funkcija
        $.ajax({
            url: '/student',
            type: 'POST',
            data: JSON.stringify(student),
            contentType: 'application/json',
            success: function(response) {
                fnCallback(null, response);
            },
            error: function(response) {
                console.log(response, null);
            }
        });
    }

    const postaviGrupu = function(index, grupa, fnCallback) {
        //index je string, grupa string, fnCallback je funkcija
        $.ajax({
            url: '/student/' + index,
            type: 'PUT',
            data: JSON.stringify({grupa: grupa}),
            contentType: 'application/json',
            success: function(response) {
                fnCallback(null, response);
            },
            error: function(response) {
                console.log(response, null);
            }
        });
    }

    const dodajBatch = function(csvStudenti, fnCallback) {
        //csvStudenti je string, fnCallback je funkcija
        $.ajax({
            url: '/batch/student',
            type: 'POST',
            data: csvStudenti,
            contentType: 'text/plain',
            success: function(response) {
                fnCallback(null, response);
            },
            error: function(response) {
                console.log(response, null);
            }
        });
    }


    return {
        dodajStudenta : dodajStudenta,
        postaviGrupu : postaviGrupu,
        dodajBatch : dodajBatch
    }
})();
