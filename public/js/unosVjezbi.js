// $(document).ready(function () {
//     $("#dodaj").click(function () {
//         const brojVjezbi = $("#brojVjezbi").val();
//         VjezbeAjax.dodajInputPolja(document.getElementById('unosiZadataka'), brojVjezbi);
//     });
//
//     $("#posaljiZadatke").click(function () {
//         const brojVjezbi = $("#brojVjezbi").val();
//         const zadaci = [];
//         for (let i = 0; i < brojVjezbi; i++) {
//             zadaci.push($("#unosiZadataka").children().eq(i).val());
//         }
//         VjezbeAjax.posaljiZadatke(zadaci, function(err, data) {
//             if (err) {
//                 alert(err);
//             } else {
//                 console.log(data);
//             }
//         });
//     });
// });

window.onload = function() {
    const dodaj = document.getElementById('dodaj');
    dodaj.addEventListener('click', function() {
        const brojVjezbi = document.getElementById('brojVjezbi').value;
        if(brojVjezbi < 0 || brojVjezbi > 15) {
            alert('Broj vjezbi mora biti između 1 i 15');
            while (document.getElementById('unosiZadataka').firstChild) {
                document.getElementById('unosiZadataka').removeChild(document.getElementById('unosiZadataka').lastChild);
            }
            return;
        }
        VjezbeAjax.dodajInputPolja(document.getElementById('unosiZadataka'), brojVjezbi);

        const posalji = document.getElementById('posaljiZadatke');
        posalji.addEventListener('click', function() {
            const brojVjezbi = document.getElementById('brojVjezbi').value;
            if(brojVjezbi < 1 || brojVjezbi > 15) alert('Broj vjezbi mora biti između 1 i 15.');
                const zadaci = [];
                for (let i = 0; i < brojVjezbi; i++) {
                    if(document.getElementById('unosiZadataka').children[i].value === '') {
                        alert('Sva polja moraju biti popunjena.');
                        return;
                    } else if(document.getElementById('unosiZadataka').children[i].value < 0 ||
                        document.getElementById('unosiZadataka').children[i].value > 10) {
                        alert('Zadatak mora biti izmedju 0 i 10.');
                        return;
                    } else zadaci.push(Number.parseInt(document.getElementById('z' + i).value));
                }
                let temp = {
                    brojVjezbi: Number.parseInt(brojVjezbi),
                    brojZadataka: zadaci
                }
                console.log(temp);
                VjezbeAjax.posaljiPodatke(temp, function (err, data) {
                });
        });
    });
}