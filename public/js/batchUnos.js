window.onload = () => {
    const posalji = document.getElementById('posalji');
    posalji.addEventListener('click', () => {
        const unos = document.getElementById('unos');
        StudentAjax.dodajBatch(
            unos.value,
            (err, data) => {
                if (err) {
                    console.log(err.status);
                    document.getElementById('ajaxstatus').innerHTML = err.status;
                } else {
                    console.log(data.status);
                    document.getElementById('ajaxstatus').innerHTML = data.status;
                }
            });
    });
}