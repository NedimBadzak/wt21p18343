let assert = chai.assert;
describe('TestoviParser', function () {
    describe('dajTacnost()', function () {
        it('3 tests, all tests passed', function () {

            var query = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 3,
                    "pending": 0,
                    "failures": 0,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [],
                "passes": [
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });
            let result = TestoviParser.dajTacnost(query);
            assert.equal(JSON.stringify(result), "{\"tacnost\":\"100%\",\"greske\":[]}");

        });
        it('3 tests, 1 failure', function () {

            var query = JSON.stringify({
                    "stats": {
                        "suites": 3,
                        "tests": 3,
                        "passes": 2,
                        "pending": 0,
                        "failures": 1,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 9
                    },
                    "tests": [
                        {
                            "title": "should draw 7 rows when parameter are 4,3",
                            "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 4,3",
                            "file": null,
                            "duration": 1,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "should draw 3 rows when parameter are 2,3",
                            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                            "file": null,
                            "duration": 1,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "should draw 2 columns in row 2 when parameter are 2,3",
                            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                            "file": null,
                            "duration": 0,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [
                        {
                            "title": "should draw 3 rows when parameter are 2,3",
                            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                            "file": null,
                            "duration": 1,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "passes": [
                        {
                            "title": "should draw 7 rows when parameter are 4,3",
                            "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                            "file": null,
                            "duration": 1,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "should draw 2 columns in row 2 when parameter are 2,3",
                            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                            "file": null,
                            "duration": 0,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ]
                }
            );
            let result = TestoviParser.dajTacnost(query);
            assert.equal(JSON.stringify(result), JSON.stringify({
                "tacnost": "66.7%",
                "greske": ["Tabela crtaj() should draw 3 rows when parameter are 2,3"]
            }));
        });
        it('3 tests, 2 failures', function () {

            var query = JSON.stringify({
                    "stats": {
                        "suites": 3,
                        "tests": 3,
                        "passes": 1,
                        "pending": 0,
                        "failures": 2,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 9
                    },
                    "tests": [
                        {
                            "title": "should draw 7 rows when parameter are 4,3",
                            "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                            "file": null,
                            "duration": 1,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "should draw 3 rows when parameter are 2,3",
                            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                            "file": null,
                            "duration": 1,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "should draw 2 columns in row 2 when parameter are 2,3",
                            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                            "file": null,
                            "duration": 0,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [{
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                        {
                            "title": "should draw 2 columns in row 2 when parameter are 2,3",
                            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                            "file": null,
                            "duration": 0,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "passes": [{
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }]
                }
            );
            let result = TestoviParser.dajTacnost(query);
            assert.equal(JSON.stringify(result), JSON.stringify({
                "tacnost": "33.3%",
                "greske": ["Tabela crtaj() should draw 3 rows when parameter are 2,3", "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3"]
            }));
        });
        it('3 tests, 3 failures', function () {

            var query = JSON.stringify({
                    "stats": {
                        "suites": 3,
                        "tests": 3,
                        "passes": 0,
                        "pending": 0,
                        "failures": 3,
                        "start": "2021-11-05T15:00:26.343Z",
                        "end": "2021-11-05T15:00:26.352Z",
                        "duration": 9
                    },
                    "tests": [
                        {
                            "title": "should draw 7 rows when parameter are 4,3",
                            "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                            "file": null,
                            "duration": 1,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "should draw 3 rows when parameter are 2,3",
                            "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                            "file": null,
                            "duration": 1,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "should draw 2 columns in row 2 when parameter are 2,3",
                            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                            "file": null,
                            "duration": 0,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "pending": [],
                    "failures": [{
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                        {
                            "title": "should draw 2 columns in row 2 when parameter are 2,3",
                            "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                            "file": null,
                            "duration": 0,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        },
                        {
                            "title": "should draw 7 rows when parameter are 4,3",
                            "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                            "file": null,
                            "duration": 1,
                            "currentRetry": 0,
                            "speed": "fast",
                            "err": {}
                        }
                    ],
                    "passes": []
                }
            );
            let result = TestoviParser.dajTacnost(query);
            assert.equal(JSON.stringify(result), JSON.stringify({
                "tacnost": "0%",
                "greske": ["Tabela crtaj() should draw 3 rows when parameter are 2,3", "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3", "Tabela crtaj() should draw 7 rows when parameter are 4,3"]
            }));
        });
        it('Can\'t execute tests', function () {

            let result = TestoviParser.dajTacnost("blabla");
            assert.equal(JSON.stringify(result), JSON.stringify({
                "tacnost": "0%",
                "greske": ["Testovi se ne mogu izvršiti"]
            }));
        });
        it('Missing pending, Can\'t execute tests', function () {

            let result = TestoviParser.dajTacnost(JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 0,
                    "failures": 3,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "failures": [{
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "passes": []
            }));
            assert.equal(JSON.stringify(result), JSON.stringify({
                "tacnost": "0%",
                "greske": ["Testovi se ne mogu izvršiti"]
            }));
        });
        it('Missing speed inside tests, Can\'t execute tests', function () {

            let result = TestoviParser.dajTacnost(JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 0,
                    "pending": 0,
                    "failures": 3,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "failures": [{
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "passes": []
            }));
            assert.equal(JSON.stringify(result), JSON.stringify({
                "tacnost": "0%",
                "greske": ["Testovi se ne mogu izvršiti"]
            }));
        });


    });
});