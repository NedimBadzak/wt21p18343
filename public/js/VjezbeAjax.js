let VjezbeAjax = (function() {
    let dodaneVjezbe = [];

    function ValidirajBrojVjezbi(brojVjezbi) {
        return brojVjezbi >= 1 &&
        brojVjezbi <= 15 &&
        Number.isInteger(parseFloat(brojVjezbi));
    }


    // function ValidirajObjekat(objekat) {
    //     return objekat.brojVjezbi >= 1 &&
    //         objekat.brojVjezbi <= 15 &&
    //         !isNaN(parseInt(objekat.brojVjezbi)) &&
    //         objekat.brojZadataka.length == objekat.brojVjezbi &&
    //         objekat.brojZadataka.every((zadatak) => {
    //             return zadatak >= 0 &&
    //                 zadatak <= 10 &&
    //                 !isNaN(parseInt(zadatak));
    //         });
    // }

    function ValidirajObjekat2(objekat) {
        let data = objekat.brojVjezbi + "";
        let zadaci = objekat.brojZadataka;
        let greska = {
            status : "error",
            data : "Pogrešan parametar "
        }
        let pravilneVjezbe = objekat.brojVjezbi >= 1 &&
            objekat.brojVjezbi <= 15 &&
            Number.isInteger(objekat.brojVjezbi);
        if(!pravilneVjezbe) greska.data += "brojVjezbi";


        let nepravilanNeki = false;

        for(let i=0; i<objekat.brojVjezbi; i++) {
            if(zadaci[i] < 0 || zadaci[i] > 10) {
                if(!pravilneVjezbe || nepravilanNeki) greska.data += ",";
                greska.data += "z" + i;
                nepravilanNeki = true;
            }
            data += ', ' + zadaci[i];
        }

        let pravilniZadaci = zadaci.length === objekat.brojVjezbi;
        if(!pravilniZadaci) {
            if (!pravilneVjezbe || nepravilanNeki) greska.data += ",";
            greska.data += "brojZadataka";
        }

        if(!pravilniZadaci || !pravilneVjezbe || nepravilanNeki) {
            return greska;
        } else return null;
    }

    const dodajInputPolja = (function(DOMelementDIVauFormi, brojVjezbi) {
        // if(DOMelementDIVauFormi == null) {
        //     return;
        // }
        while (DOMelementDIVauFormi.firstChild) {
            DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastChild);
        }
        if(!ValidirajBrojVjezbi(brojVjezbi)) {
            return;
        }
        for(let i=0; i<brojVjezbi; i++) {
            let labela = document.createElement("label");
            labela.setAttribute("for", "z" + i);
        let inputPolja = document.createElement("input");
        inputPolja.setAttribute("type", "text");
        inputPolja.setAttribute("id", "z" + i);
        inputPolja.setAttribute("name", "z" + i);
        inputPolja.setAttribute("value",  "4");
        DOMelementDIVauFormi.appendChild(labela);
        DOMelementDIVauFormi.appendChild(inputPolja);
        DOMelementDIVauFormi.appendChild(document.createElement("br"));
        }
        let posaljiZadatke = document.createElement("input");
        posaljiZadatke.setAttribute("type", "button");
        posaljiZadatke.setAttribute("id", "posaljiZadatke");
        posaljiZadatke.setAttribute("value", "Posalji zadatke");
        DOMelementDIVauFormi.appendChild(posaljiZadatke);
    });

    const posaljiPodatke = function (vjezbeObjekat, callbackFja) {
        // if(!ValidirajObjekat(vjezbeObjekat)) {
        //     return callbackFja("Neispravni podaci", null);
        // }
        $.ajax({
            type: "POST",
            url: "/vjezbe",
            data: JSON.stringify(vjezbeObjekat),
            contentType: "application/json",
            success: (response) => {
                if(ValidirajObjekat2(response) == null) callbackFja(null, response);
                else callbackFja(ValidirajObjekat2(response), null);
            },
            error: (error) => {
                callbackFja(error, null);
            }
        });
    }

    const dohvatiPodatke = function(callbackFja) {
        $.ajax({
            type: "GET",
            url: "/vjezbe",
            success: (response) => {
                if(ValidirajObjekat2(response) == null) callbackFja(null, response);
                else callbackFja(ValidirajObjekat2(response), null);
            },
            error: (error) => {
                callbackFja(error, null);
            }
        });
    }

    const dohvatiPodatke2 = function(callbackFja) {
        $.ajax({
            type: "GET",
            url: "/vjezbe",
            success: (response) => {
                callbackFja(null, response);
            },
            error: (error) => {
                callbackFja(error, null);
            }
        });
    }


    const iscrtajVjezbe = function(divDOMelement, objekat) {
        // if(divDOMelement == null) {
        //     return;
        // }
        if(ValidirajObjekat2(objekat) != null) {
            return;
        }
        let brojVjezbi = objekat.brojVjezbi;
        for(let i=0; i<brojVjezbi; i++) {
            let vjezbaZadatakWrapper = document.createElement("div");
            let vjezba = document.createElement('button');
            vjezba.setAttribute("class", "vjezba");
            vjezba.innerText = "Vjezba " + (i+1);
            vjezba.onclick = () => iscrtajZadatke(vjezbaZadatakWrapper, objekat.brojZadataka[i]);
            vjezbaZadatakWrapper.appendChild(vjezba);
            divDOMelement.appendChild(vjezbaZadatakWrapper);
        }
    }

    function iscrtajZadatke(divDOMelement, brojZadataka) {
        if(divDOMelement == null) {
            return;
        }
        if(!dodaneVjezbe.includes(divDOMelement)) {
            for(let i=0; i<dodaneVjezbe.length; i++) dodaneVjezbe[i].children[1].style.display = "none";
            dodaneVjezbe.push(divDOMelement);
            let zadaci = document.createElement('div');
            zadaci.setAttribute("id", "zadaci");
            for (let i = 0; i < brojZadataka; i++) {
                let zadatak = document.createElement('div');
                zadatak.setAttribute("class", "zadatak");
                zadatak.innerText = "Zadatak " + (i + 1);
                zadaci.appendChild(zadatak);
            }
            divDOMelement.appendChild(zadaci);
        } else {
            for(let i=0; i<dodaneVjezbe.length; i++) dodaneVjezbe[i].children[1].style.display = "none";
            divDOMelement.children[1].style.display = "grid";
        }
    }


    return {
        dodajInputPolja: dodajInputPolja,
        posaljiPodatke: posaljiPodatke,
        dohvatiPodatke: dohvatiPodatke,
        iscrtajVjezbe: iscrtajVjezbe
    };
}());