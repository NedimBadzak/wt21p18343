let assert = chai.assert;
describe('TestoviParser', function () {
    describe('porediRezultate()', function () {
        it('All tests are the same, no errors', function () {
            var query1 = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 3,
                    "pending": 0,
                    "failures": 0,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [],
                "passes": [
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            var query2 = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 3,
                    "pending": 0,
                    "failures": 0,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [],
                "passes": [
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            let result = TestoviParser.porediRezultate(query1, query2);
            assert.equal(JSON.stringify(result), "{\"promjena\":\"100%\",\"greske\":[]}");

        });

        it('All tests are the same, but 2 errors in the second result', function () {
            var query1 = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 3,
                    "pending": 0,
                    "failures": 0,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [],
                "passes": [
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            var query2 = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 1,
                    "pending": 0,
                    "failures": 2,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [{
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "passes": [

                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            let result = TestoviParser.porediRezultate(query1, query2);
            assert.equal(JSON.stringify(result), "{\"promjena\":\"33.3%\",\"greske\":[\"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\"]}");
        });

        it('All tests are the same, but 1 error in first result, 2 errors in the second result', function () {
            var query1 = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 2,
                    "pending": 0,
                    "failures": 1,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [{
                    "title": "should draw 7 rows when parameter are 4,3",
                    "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
                "passes": [
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            var query2 = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 1,
                    "pending": 0,
                    "failures": 2,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [{
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "passes": [

                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            let result = TestoviParser.porediRezultate(query1, query2);
            assert.equal(JSON.stringify(result), "{\"promjena\":\"33.3%\",\"greske\":[\"Tabela crtaj() should draw 3 rows when parameter are 2,3\",\"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\"]}");
        });


        it('1 test different in result 1, 1 test different in result 2 and are failing', function () {
            var query1 = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 2,
                    "pending": 0,
                    "failures": 1,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 6 rows when parameter are 3,3",
                        "fullTitle": "Tabela crtaj() should draw 6 rows when parameter are 3,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [{
                    "title": "should draw 6 rows when parameter are 3,3",
                    "fullTitle": "Tabela crtaj() should draw 6 rows when parameter are 3,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
                "passes": [
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            var query2 = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 1,
                    "pending": 0,
                    "failures": 2,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [{
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "passes": [

                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            let result = TestoviParser.porediRezultate(query1, query2);
            assert.equal(JSON.stringify(result), "{\"promjena\":\"75%\",\"greske\":[\"Tabela crtaj() should draw 6 rows when parameter are 3,3\",\"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\"Tabela crtaj() should draw 3 rows when parameter are 2,3\"]}");
        });

        it('2 tests different in result 1, 3 tests different in result 2 and 2 are failing', function () {
            var query1 = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 1,
                    "pending": 0,
                    "failures": 2,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 4 rows when parameter are 3,1",
                        "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 3,1",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 6 rows when parameter are 3,3",
                        "fullTitle": "Tabela crtaj() should draw 6 rows when parameter are 3,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [{
                    "title": "should draw 6 rows when parameter are 3,3",
                    "fullTitle": "Tabela crtaj() should draw 6 rows when parameter are 3,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }, {
                    "title": "should draw 4 rows when parameter are 3,1",
                    "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 3,1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
                "passes": [
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            var query2 = JSON.stringify({
                "stats": {
                    "suites": 4,
                    "tests": 4,
                    "passes": 2,
                    "pending": 0,
                    "failures": 2,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },

                    {
                        "title": "should draw 8 rows when parameter are 4,4",
                        "fullTitle": "Tabela crtaj() should draw 8 rows when parameter are 4,4",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 9 rows when parameter are 4,5",
                        "fullTitle": "Tabela crtaj() should draw 9 rows when parameter are 4,5",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [{
                    "title": "should draw 7 rows when parameter are 4,3",
                    "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },

                    {
                        "title": "should draw 8 rows when parameter are 4,4",
                        "fullTitle": "Tabela crtaj() should draw 8 rows when parameter are 4,4",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "passes": [

                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 9 rows when parameter are 4,5",
                        "fullTitle": "Tabela crtaj() should draw 9 rows when parameter are 4,5",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            let result = TestoviParser.porediRezultate(query1, query2);
            assert.equal(JSON.stringify(result), "{\"promjena\":\"66.7%\",\"greske\":[\"Tabela crtaj() should draw 4 rows when parameter are 3,1\",\"Tabela crtaj() should draw 6 rows when parameter are 3,3\",\"Tabela crtaj() should draw 7 rows when parameter are 4,3\",\"Tabela crtaj() should draw 8 rows when parameter are 4,4\"]}");
        });

        it('2 tests different in result 1, 3 tests different in result 2 and 3 are failing', function () {
            var query1 = JSON.stringify({
                "stats": {
                    "suites": 3,
                    "tests": 3,
                    "passes": 1,
                    "pending": 0,
                    "failures": 2,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 4 rows when parameter are 3,1",
                        "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 3,1",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 6 rows when parameter are 3,3",
                        "fullTitle": "Tabela crtaj() should draw 6 rows when parameter are 3,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [{
                    "title": "should draw 6 rows when parameter are 3,3",
                    "fullTitle": "Tabela crtaj() should draw 6 rows when parameter are 3,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }, {
                    "title": "should draw 4 rows when parameter are 3,1",
                    "fullTitle": "Tabela crtaj() should draw 4 rows when parameter are 3,1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
                "passes": [
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            var query2 = JSON.stringify({
                "stats": {
                    "suites": 4,
                    "tests": 4,
                    "passes": 2,
                    "pending": 0,
                    "failures": 2,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [
                    {
                        "title": "should draw 7 rows when parameter are 4,3",
                        "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },

                    {
                        "title": "should draw 8 rows when parameter are 4,4",
                        "fullTitle": "Tabela crtaj() should draw 8 rows when parameter are 4,4",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },
                    {
                        "title": "should draw 9 rows when parameter are 4,5",
                        "fullTitle": "Tabela crtaj() should draw 9 rows when parameter are 4,5",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "pending": [],
                "failures": [{
                    "title": "should draw 7 rows when parameter are 4,3",
                    "fullTitle": "Tabela crtaj() should draw 7 rows when parameter are 4,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                    {
                        "title": "should draw 2 columns in row 2 when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    },

                    {
                        "title": "should draw 8 rows when parameter are 4,4",
                        "fullTitle": "Tabela crtaj() should draw 8 rows when parameter are 4,4",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ],
                "passes": [
                    {
                        "title": "should draw 9 rows when parameter are 4,5",
                        "fullTitle": "Tabela crtaj() should draw 9 rows when parameter are 4,5",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }
                ]
            });

            let result = TestoviParser.porediRezultate(query1, query2);
            assert.equal(JSON.stringify(result), "{\"promjena\":\"66.7%\",\"greske\":[\"Tabela crtaj() should draw 4 rows when parameter are 3,1\",\"Tabela crtaj() should draw 6 rows when parameter are 3,3\",\"Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3\",\"Tabela crtaj() should draw 7 rows when parameter are 4,3\",\"Tabela crtaj() should draw 8 rows when parameter are 4,4\"]}");
        });
        it('2 tests different in result 1, 2 tests different in result 2 and the common 1 is failing', function () {

            let query1 = {
                "stats": {
                    "suites": 2,
                    "tests": 3,
                    "passes": 0,
                    "pending": 0,
                    "failures": 3,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [{
                    "title": "should draw 3 rows when parameterare 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }, {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }, {
                    "title": "should draw 5 columns in row 2 when parameter are 4,1",
                    "fullTitle": "Tabela crtaj() should draw 5 columns in row 2 when parameter are 4,1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
                "pending": [],
                "failures": [
                    {
                        "title": "should draw 3 rows when parameter are 2,3",
                        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                        "file": null,
                        "duration": 1,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }],
                "passes": [{
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }, {
                    "title": "should draw 5 columns in row 2 when parameter are 4,1",
                    "fullTitle": "Tabela crtaj() should draw 5 columns in row 2 when parameter are 4,1",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }]
            };
            let query2 = {
                "stats": {
                    "suites": 2,
                    "tests": 2,
                    "passes": 1,
                    "pending": 0,
                    "failures": 1,
                    "start": "2021-11-05T15:00:26.343Z",
                    "end": "2021-11-05T15:00:26.352Z",
                    "duration": 9
                },
                "tests": [{
                    "title": "should draw 3 rows when parameterare 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }, {
                    "title": "should draw 4 columns in row 2 when parameter are 2,2",
                    "fullTitle": "Tabela crtaj() should draw 4 columns in row 2 when parameter are 2,2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                    {
                        "title": "should draw 6 columns in row 2 when parameter are 3,3",
                        "fullTitle": "Tabela crtaj() should draw 6 columns in row 2 when parameter are 3,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }],
                "pending": [],
                "failures": [{
                    "title": "should draw 3 rows when parameterare 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
                "passes": [{
                    "title": "should draw 4 columns in row 2 when parameter are 2,2",
                    "fullTitle": "Tabela crtaj() should draw 4 columns in row 2 when parameter are 2,2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                    {
                        "title": "should draw 6 columns in row 2 when parameter are 3,3",
                        "fullTitle": "Tabela crtaj() should draw 6 columns in row 2 when parameter are 3,3",
                        "file": null,
                        "duration": 0,
                        "currentRetry": 0,
                        "speed": "fast",
                        "err": {}
                    }]
            };

            let result = TestoviParser.porediRezultate(JSON.stringify(query1), JSON.stringify(query2));
            assert.equal(JSON.stringify(result), "{\"promjena\":\"50%\",\"greske\":[\"Tabela crtaj() should draw 3 rows when parameter are 2,3\"]}");
        });


    });
});