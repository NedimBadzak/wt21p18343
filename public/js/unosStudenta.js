window.onload = () => {
    const posalji = document.getElementById('posalji');
    posalji.addEventListener('click', () => {
        const ime = document.getElementById('ime');
        const prezime = document.getElementById('prezime');
        const index = document.getElementById('index');
        const grupa = document.getElementById('grupa');
        StudentAjax.dodajStudenta({
            ime: ime.value,
            prezime: prezime.value,
            index: index.value,
            grupa: grupa.value
        }, (err, data) => {
            if (err) {
                console.log(err.status);
                document.getElementById('ajaxstatus').innerHTML = err.status;
            } else {
                console.log(data.status);
                document.getElementById('ajaxstatus').innerHTML = data.status;
            }
        });
    });
}