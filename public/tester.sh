#!/bin/sh

cd testovi/wt21testovispirale/S1

galen check z1Desktop.gspec --url http://wt.com/$1.html --htmlreport izvjestajz1desktop --size 1920x1080 -Dwebdriver.chrome.driver=chromedriver
galen check z1Tablet.gspec --url http://wt.com/$1.html --htmlreport izvjestajz1tablet --size 700x1024 -Dwebdriver.chrome.driver=chromedriver
galen check z1Mobile.gspec --url http://wt.com/$1.html --htmlreport izvjestajz1mobile --size 350x700 -Dwebdriver.chrome.driver=chromedriver
