const Sequelize = require('sequelize');
const sequelize = require('./baza');
let chai = require("chai");
let chaiHttp = require("chai-http");
let assert = chai.assert;
chai.use(chaiHttp);
chai.should();
//

var Student = require("./models/Student")(sequelize);
var Vjezba = require("./models/Vjezba")(sequelize);
var Grupa = require("./models/Grupa")(sequelize);
var Zadatak = require("./models/Zadatak")(sequelize);

Vjezba.hasMany(Zadatak, {as: "zadaciVjezbe"});
Grupa.hasMany(Student, {as: "studentiGrupe"});

//
// async function obrisi(tabela) {
//     chai.request("http://localhost:3000")
//         .get("/delete/" + tabela)
//         .end((err, res) => {
//             if (err) {
//                 console.log(err);
//             }
//             res.should.have.status(200);
//         });
// }

Array.prototype.forEachAsync = async function (fn) {
    for (let t of this) {
        await fn(t)
    }
}

//
// async function ubaciPeru() {
//     chai.request("http://localhost:3000")
//         .post("/student")
//         .set('content-type', 'application/json')
//         .send({
//             ime: "Pera",
//             prezime: "Peric",
//             index: "18121",
//             grupa: "PG1"
//         })
//         .end((err, res) => {
//             if (err) {
//                 console.log(err);
//             }
//             res.should.have.status(200);
//         });
// }

describe('Student testovi', () => {
    describe('POST student', function () {
        // before(function () {
        //     obrisi("Students").then(() => obrisi("Grupas"));
        // });
        let studentiIzDB = [];
        let grupeIzDB = [];
        let vjezbeIzDB = [];
        let zadaciIzDB = [];
        this.beforeAll((done) => {
            // () => obrisi("Students").then(() => obrisi("Grupas"))
            Student.findAll().then(studenti => {
                studentiIzDB = studenti;
            }).then(() => Grupa.findAll().then(grupe => {
                grupeIzDB = grupe;
            })).then(() => Vjezba.findAll().then(vjezbe => {
                vjezbeIzDB = vjezbe;
            })).then(() => Zadatak.findAll().then(zadaci => {
                zadaciIzDB = zadaci;
            })).then(() => sequelize.sync({force: true}).then(() => done()));


        });
        this.afterAll((done) => {
            Grupa.bulkCreate(grupeIzDB).then(() => Student.bulkCreate(studentiIzDB)).then(() => Vjezba.bulkCreate(vjezbeIzDB)).then(() => Zadatak.bulkCreate(zadaciIzDB)).then(() => done());
            this.timeout(500);
        });


        afterEach((done) => {
            sequelize.sync({ force: true }).then(() => { done() });
            this.timeout(300);
        });


        it('Test ubacivanja studenta, sve ok, ne postoji grupa', async function () {
            await chai.request('http://localhost:3000')
                .post('/student')
                .set('content-type', 'application/json')
                .send({
                    ime: "Pera",
                    prezime: "Peric",
                    index: "18121",
                    grupa: "PG1"
                })
                .then(function (res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.deep.equal({status: "Kreiran student!"});
                });
        })

        it('Test ubacivanja studenta, vec postoji, drugacija grupa', async function () {
            await chai.request('http://localhost:3000')
                .post('/student')
                .set('content-type', 'application/json')
                .send({
                    ime: "Neda",
                    prezime: "Nedic",
                    index: "18121",
                    grupa: "PG3"
                })
                .then(async function (res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    await chai.request('http://localhost:3000')
                        .post('/student')
                        .set('content-type', 'application/json')
                        .send({
                            ime: "Pera",
                            prezime: "Peric",
                            index: "18121",
                            grupa: "PG8"
                        }).then(async function (res) {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.deep.equal({status: "Student sa indexom 18121 već postoji!"});
                            await Grupa.findOne({where: {imeGrupe: "PG8"}}).then(grupa => {
                                assert(grupa == null, "Grupa postoji a ne bi smjela")
                            });
                        });
                });
        })

        it('Test ubacivanja studenta, vec postoji, ista grupa', async function () {
            await chai.request('http://localhost:3000')
                .post('/student')
                .set('content-type', 'application/json')
                .send({
                    ime: "Neda",
                    prezime: "Nedic",
                    index: "18121",
                    grupa: "PG3"
                })
                .then(async function (res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    await chai.request('http://localhost:3000')
                        .post('/student')
                        .set('content-type', 'application/json')
                        .send({
                            ime: "Pera",
                            prezime: "Peric",
                            index: "18121",
                            grupa: "PG8"
                        }).then(async function (res) {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.deep.equal({status: "Student sa indexom 18121 već postoji!"});
                            await Grupa.findOne({where: {imeGrupe: "PG3"}}).then(grupa => {
                                console.log(grupa);
                                assert(grupa !== null, "Grupa ne postoji, a trebala bi")
                            });
                        });
                });
        })

        it('Test ubacivanja studenta, prazan', async function () {
            await chai.request('http://localhost:3000')
                .post('/student')
                .set('content-type', 'application/json')
                .send({
                    ime: "",
                    prezime: "",
                    index: "",
                    grupa: ""
                })
                .then(async function (res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    await Student.findAll().then(studenti => {
                        assert(studenti.length === 0, "Studenti postoje, a ne bi trebali")
                    });
                    // res.body.should.deep.equal({status: "Nemoguce"});
                });
        })

        it('Test ubacivanja studenta, sve ok, postoji grupa', async function () {
            await chai.request('http://localhost:3000')
                .post('/student')
                .set('content-type', 'application/json')
                .send({
                    ime: "Neda",
                    prezime: "Nedic",
                    index: "21312",
                    grupa: "PG1"
                })
                .then(function (res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.deep.equal({status: "Kreiran student!"});
                });
        })
    });

    describe('PUT student', function () {
        // before(function () {
        //     obrisi("Students").then(() => obrisi("Grupas"));
        // });
        let studentiIzDB = [];
        let grupeIzDB = [];
        let vjezbeIzDB = [];
        let zadaciIzDB = [];
        this.beforeAll((done) => {
            // () => obrisi("Students").then(() => obrisi("Grupas"))
            Student.findAll().then(studenti => {
                studentiIzDB = studenti;
            }).then(() => Grupa.findAll().then(grupe => {
                grupeIzDB = grupe;
            })).then(() => Vjezba.findAll().then(vjezbe => {
                vjezbeIzDB = vjezbe;
            })).then(() => Zadatak.findAll().then(zadaci => {
                zadaciIzDB = zadaci;
            })).then(() => sequelize.sync({force: true}).then(() => done()));


        });
        this.afterAll((done) => {
            Grupa.bulkCreate(grupeIzDB).then(() => Student.bulkCreate(studentiIzDB)).then(() => Vjezba.bulkCreate(vjezbeIzDB)).then(() => Zadatak.bulkCreate(zadaciIzDB)).then(() => done());
            this.timeout(500);
        });


        afterEach((done) => {
            sequelize.sync({ force: true }).then(() => { done() });
            this.timeout(300);
        });

        it('Test mijenjanja grupe studenta, sve ok, ne postoji grupa', async function () {
            await Student.create({
                ime: "Pera",
                prezime: "Peric",
                index: "18125",
                grupa: "PG1"
            }).then(async () => {
                await chai.request('http://localhost:3000')
                    .put('/student' + '/18125')
                    .set('content-type', 'application/json')
                    .send({
                        grupa: "PG2"
                    }).then((res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.deep.equal({status: "Promjenjena grupa studentu 18125"});
                    });
            });
        })

        it('Test mijenjanja grupe studenta, prazna grupa', async function () {
            await Student.create({
                ime: "Pera",
                prezime: "Peric",
                index: "18125",
                grupa: "PG1"
            }).then(async () => {
                await chai.request('http://localhost:3000')
                    .put('/student' + '/18125')
                    .set('content-type', 'application/json')
                    .send({
                        grupa: ""
                    }).then((res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        // res.body.should.deep.equal({status: "Promjenjena grupa studentu 18125"});
                    });
            });
        })
        //
        it('Test mijenjanja grupe studenta, sve ok, postoji grupa', async function () {
            await Student.create({
                ime: "Nedo",
                prezime: "Nedic",
                index: "12345",
                grupa: "PG2"
            }).then(async (res) => {
                await chai.request('http://localhost:3000')
                    .put('/student' + '/12345')
                    .set('content-type', 'application/json')
                    .send({
                        grupa: "PG2"
                    }).then((res) => {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.deep.equal({status: "Promjenjena grupa studentu 12345"});
                    });
            });
        })

        it('Test mijenjanja grupe studenta, ne postoji student', async function () {

            await chai.request('http://localhost:3000')
                .put('/student' + '/11111')
                .set('content-type', 'application/json')
                .send({
                    grupa: "PG2"
                }).then((res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.deep.equal({status: "Student sa indexom 11111 ne postoji"});
                });
        })
    });

    describe('Dodaj batch studente', async function () {
        // before(
        //     () => obrisi("Students").then(() => obrisi("Grupas"))
        // )

        let studentiIzDB = [];
        let grupeIzDB = [];
        let vjezbeIzDB = [];
        let zadaciIzDB = [];
        this.beforeAll((done) => {
            // () => obrisi("Students").then(() => obrisi("Grupas"))
            Student.findAll().then(studenti => {
                studentiIzDB = studenti;
            }).then(() => Grupa.findAll().then(grupe => {
                grupeIzDB = grupe;
            })).then(() => Vjezba.findAll().then(vjezbe => {
                vjezbeIzDB = vjezbe;
            })).then(() => Zadatak.findAll().then(zadaci => {
                zadaciIzDB = zadaci;
            })).then(() => sequelize.sync({force: true}).then(() => done()));


        });
        this.afterAll((done) => {
            Grupa.bulkCreate(grupeIzDB).then(() => Student.bulkCreate(studentiIzDB)).then(() => Vjezba.bulkCreate(vjezbeIzDB)).then(() => Zadatak.bulkCreate(zadaciIzDB)).then(() => done());
            this.timeout(500);
        });


        afterEach((done) => {
            sequelize.sync({ force: true }).then(() => { done() });
            this.timeout(300);
        });

        it('Test batch ubacivanja 5 studenta', async function () {
            await chai.request('http://localhost:3000')
                .post('/batch/student')
                .set('content-type', 'text/plain')
                .send("Irfan,Irfic,133456,Saradnik\n" +
                    "Nedimic,Badzakic,987654,PON\n" +
                    "Vensada,Vensadic,123456,Saradnik\n" +
                    "Dzana,Dzanic,132465,PG2-1A\n" +
                    "John,Smith,165432,PON")
                .then(function (res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.deep.equal({status: "Dodano 5 studenata!"});
                });
        })

        it('Test batch ubacivanja 3 studenta i 2 postoje', async function () {
            await chai.request('http://localhost:3000')
                .post('/batch/student')
                .set('content-type', 'text/plain')
                .send("Irfan,Irfic,133456,Saradnik\n" +
                    "Nedimic,Badzakic,987654,PON\n" +
                    "Vensada,Vensadic,123456,Saradnik\n" +
                    "Dzana,Dzanic,132465,PG2-1A\n" +
                    "John,Smith,165432,PON").then(async () => {
                    await chai.request('http://localhost:3000')
                        .post('/batch/student')
                        .set('content-type', 'text/plain')
                        .send("Irfan,Irfic,133456,Saradnik\n" +
                            "Meho,Mehic,16543,Svjedok\n" +
                            "Nedimic,Badzakic,987654,PON\n" +
                            "Adisa,Adisic,521312,PG2-1A\n" +
                            "Zlatan,Zlatanic,653231,ATE1")
                        .then(function (res) {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.deep.equal({status: "Dodano 3 studenata, a studenti 133456,987654 već postoje!"});
                        });
                });

            // Student.bulkCreate([
            //     {ime: "Irfan", prezime: "Irfic", index: "133456", grupa: "Saradnik"},
            //     {ime: "Nedimic", prezime: "Badzakic", index: "987654", grupa: "PON"},
            //     {ime: "Vensada", prezime: "Vensadic", index: "123456", grupa: "Saradnik"},
            //     {ime: "Dzana", prezime: "Dzanic", index: "132465", grupa: "PG2-1A"},
            //     {ime: "John", prezime: "Smith", index: "165432", grupa: "PON"}
            // ]).then(function (modeli) {
            //     modeli.forEachAsync(async function (model) {
            //         [grupa, created] = await Grupa.findOrCreate({where: {imeGrupe: model.grupa}});
            //         await model.update({GrupaId: grupa.id});
            //     }).then(() => {
            //         chai.request('http://localhost:3000')
            //             .post('/batch/student')
            //             .set('content-type', 'text/plain')
            //             .send("Irfan,Irfic,133456,Saradnik\n" +
            //                 "Meho,Mehic,16543,Svjedok\n" +
            //                 "Nedimic,Badzakic,987654,PON\n" +
            //                 "Samra,Samric,521312,PG2-1A\n" +
            //                 "Zlatan,Zlatanic,653231,ATE1")
            //             .end(function (err, res) {
            //                 res.should.have.status(200);
            //                 res.should.be.json;
            //                 res.body.should.deep.equal({status: "Dodano 3 studenata, a studenti 16543,521312,653231 već postoje!"});
            //                 done();
            //             });
            //     });
            // })
        })


        it('Test batch ubacivanja, svi postoje, isti redoslijed kao i dodavanje', async function () {
            await chai.request('http://localhost:3000')
                .post('/batch/student')
                .set('content-type', 'text/plain')
                .send("Irfan,Irfic,133456,Saradnik\n" +
                    "Nedimic,Badzakic,987654,PON\n" +
                    "Vensada,Vensadic,123456,Saradnik\n" +
                    "Dzana,Dzanic,132465,PG2-1A\n" +
                    "John,Smith,165432,PON")
                .then(async (res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.deep.equal({status: "Dodano 5 studenata!"});
                    await chai.request('http://localhost:3000')
                        .post('/batch/student')
                        .set('content-type', 'text/plain')
                        .send("Irfan,Irfic,133456,Saradnik\n" +
                            "Nedimic,Badzakic,987654,PON\n" +
                            "Vensada,Vensadic,123456,Saradnik\n" +
                            "Dzana,Dzanic,132465,PG2-1A\n" +
                            "John,Smith,165432,PON")
                        .then(function (res) {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.deep.equal({status: "Dodano 0 studenata, a studenti 133456,987654,123456,132465,165432 već postoje!"});
                        });
                });
        })

        it('Test batch ubacivanja, svi postoje, razlicit redoslijed u odnosu na dodavanje', async function () {
            this.timeout(5000);
            // Student.bulkCreate([
            //     {ime: "Irfan", prezime: "Irfic", index: "133456", grupa: "Saradnik"},
            //     {ime: "Nedimic", prezime: "Badzakic", index: "987654", grupa: "PON"},
            //     {ime: "Vensada", prezime: "Vensadic", index: "123456", grupa: "Saradnik"},
            //     {ime: "Dzana", prezime: "Dzanic", index: "132465", grupa: "PG2-1A"},
            //     {ime: "John", prezime: "Smith", index: "165432", grupa: "PON"}
            // ]).then(function (modeli) {
            //     modeli.forEachAsync(async function (model) {
            //         [grupa, created] = await Grupa.findOrCreate({where: {imeGrupe: model.grupa}});
            //         await model.update({GrupaId: grupa.id});
            //     }).then(() => {
            //         chai.request('http://localhost:3000')
            //             .post('/batch/student')
            //             .set('content-type', 'text/plain')
            //             .send(
            //                 "Nedimic,Badzakic,987654,PON\n" +
            //                 "Dzana,Dzanic,132465,PG2-1A\n" +
            //                 "Vensada,Vensadic,123456,Saradnik\n" +
            //                 "Irfan,Irfic,133456,Saradnik\n" +
            //                 "John,Smith,165432,PON"
            //             )
            //             .end(function (err, res) {
            //                 res.should.have.status(200);
            //                 res.should.be.json;
            //                 res.body.should.deep.equal({status: "Dodano 0 studenata, a studenti 987654,132465,123456,133456,165432 već postoje!"});
            //                 done();
            //             });
            //     });
            // });

            await chai.request('http://localhost:3000')
                .post('/batch/student')
                .set('content-type', 'text/plain')
                .send(
                    "Nedimic,Badzakic,987654,PON\n" +
                    "Dzana,Dzanic,132465,PG2-1A\n" +
                    "Vensada,Vensadic,123456,Saradnik\n" +
                    "Irfan,Irfic,133456,Saradnik\n" +
                    "John,Smith,165432,PON"
                )
                .then(async (res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.deep.equal({status: "Dodano 5 studenata!"});
                    await chai.request('http://localhost:3000')
                        .post('/batch/student')
                        .set('content-type', 'text/plain')
                        .send(
                            "Irfan,Irfic,133456,Saradnik\n" +
                            "Dzana,Dzanic,132465,PG2-1A\n" +
                            "Nedimic,Badzakic,987654,PON\n" +
                            "Vensada,Vensadic,123456,Saradnik\n" +
                            "John,Smith,165432,PON"
                        )
                        .then(function (res) {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.deep.equal({status: "Dodano 0 studenata, a studenti 133456,132465,987654,123456,165432 već postoje!"});
                        });
                });
        })

    })
})
